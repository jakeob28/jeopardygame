$(function () {
    var socket = io.connect();
    var clueValue;
    var firstBuzzer;
    var wagers = {};
    var responses = {};
    var theme = new Howl({
        src: ['audio/theme.mp3'],
        autoplay: false,
        loop: false,
        volume: 1,
    });
    var think = new Howl({
        src: ['audio/think.mp3'],
        autoplay: false,
        loop: false,
        volume: 1,
    });
    var finalding = new Howl({
        src: ['audio/finalding.mp3'],
        autoplay: false,
        loop: false,
        volume: 1,
    });
    var timeout = new Howl({
        src: ['audio/timeout.mp3'],
        autoplay: false,
        loop: false,
        volume: 1,
    });

    console.log(socket);
    $('#responsecontrols').hide();
    $('#start').click(function(){
        socket.emit('start');
        $('#start').addClass('active');
    });
    socket.on('displayclue', function (clue) {
        $('#clue').html(clue);
    });
    socket.on('answer', function (answer) {
        $('#answer').html(answer);
    });
    socket.on('deactivatebuzzers', function () {
        $('#buzzertoggle').html('Activate Buzzers').removeClass('btn-danger').addClass('btn-success');
    });
    socket.on('activatebuzzers', function () {
        $('#buzzertoggle').html('Deactivate Buzzers').removeClass('btn-success').addClass('btn-danger');
    });
    $('#correct').click(function () {
        socket.emit('givemoney', firstBuzzer, clueValue);
        $('#responsecontrols').hide();
        socket.emit('showboard');
    });
    $('#incorrect').click(function () {
        socket.emit('givemoney', firstBuzzer, clueValue * -1);
        socket.emit('activatebuzzers');
        $('#responsecontrols').hide();
    });
    $('#buzzertoggle').click(function(){
        var btn = $('#buzzertoggle');
        if (btn.html().startsWith("A")) {
            socket.emit('activatebuzzers');
            btn.html('Deactivate Buzzers').removeClass('btn-success').addClass('btn-danger');
        } else {
            socket.emit('deactivatebuzzers');
            btn.html('Activate Buzzers').removeClass('btn-danger').addClass('btn-success');
        }
    });
    $('#showBoard').click(function () {
        socket.emit('showboard');
    });
    socket.on('firstbuzz', function (name) {
        firstBuzzer = name;
        $('#firstbuzzer').html("First Buzzer: " + name + ' <a href="#" id="resetbuzz">Reset</a>');
        $('#responsecontrols').show();
    });
    $('#thememusic').click(function () {
        theme.volume(1);
        theme.play();
    });
    $('#themefade').click(function () {
        theme.fade(1, 0, 2000);
        theme.on('fade', function () {
            theme.stop();
        })
    });
    $('#thinkmusic').click(function () {
        think.volume(1);
        think.play();
    });
    $('#thinkfade').click(function () {
        think.fade(1, 0, 2000);
        think.on('fade', function () {
            think.stop();
        })
    });
    $('#timeoutmusic').click(function () {
        timeout.volume(1);
        timeout.play();
    });
    $('#timeoutfade').click(function () {
        timeout.fade(1, 0, 2000);
        timeout.on('fade', function () {
            timeout.stop();
        })
    });
    $(document).on('click', '#resetbuzz', function () {
        $('#firstbuzzer').html('');
    });

    $('button').click(function (event) {
        if (event.target.id.startsWith('cl')) {
            clueValue = event.target.id.charAt(3) * 200;
            console.log("cv " + clueValue);
            socket.emit('displayclue', event.target.id.replace('cl',''));
        }
    });

    socket.on('categories', function (categories) {
        $('#c1').html(categories[0]);
        $('#c2').html(categories[1]);
        $('#c3').html(categories[2]);
        $('#c4').html(categories[3]);
        $('#c5').html(categories[4]);
        $('#c6').html(categories[5]);
    });

    socket.on('balances', function (balances) {
        var table = '<table class="table"><thead><tr><th>Player</th><th>Balance</th></tr></thead><tbody>';
        Object.keys(balances).forEach(function (key) {
            table = table.concat('<tr><td>' + key + "</td><td>" + balances[key] + '</td></tr>');
        });
        table = table.concat('</tbody></table>');
        $('#balances').html(table);

        var options = '';
        Object.keys(balances).forEach(function (key) {
            options = options.concat('<option>' + key + '</option>');
        });
        $('#nameToUpdate').html(options);
    });

    socket.on('hideboardslot', function (boardIndex) {
       $('#cl' + boardIndex).removeClass('btn-primary').addClass('btn-outline-primary')
    });

    socket.on('wager', function (name, wager) {
        wagers[name] = wager;
        var table = '<table class="table"><thead><tr><th>Player</th><th>Wager</th></tr></thead><tbody>'
        Object.keys(wagers).forEach(function (key) {
            table = table.concat('<tr><td>' + key + "</td><td>" + wagers[key] + '</td></tr>');
        });
        $('#wagerTable').html(table);
    });
    socket.on('response', function (name, response) {
        responses[name] = response;
        var table = '<table class="table"><thead><tr><th>Player</th><th>Response</th></tr></thead><tbody>'
        Object.keys(responses).forEach(function (key) {
            table = table.concat('<tr><td>' + key + "</td><td>" + responses[key] + '</td></tr>');
        });
        $('#responseTable').html(table);
    });

    $('#balanceChangeForm').submit(function (event) {
        event.preventDefault();
        socket.emit('setbalance', $('#nameToUpdate').val(), $('#newbalance').val());
    });

    $('#displayFinal').click(function () {
        console.log('showing final j');
        socket.emit('showfinalj');
    });

    $('#displayFinalClue').click(function () {
        socket.emit('showfinalclue');
        finalding.play();
    });

    socket.on('displayfinalclue', function (clue, answer) {
        $('#clue').html(clue);
        $('#answer').html(answer);
    });

    $('#startFinal').click(function () {
        think.volume(1);
        think.play();
        socket.emit('startfinal');
    });
});