$(function () {
    var socket = io.connect();
    console.log(socket);
    console.log('Checking start status...');
    socket.on('activatebuzzers', function() {
        $('#board-container').removeClass('bg-dark').addClass('bg-light');
    });
    socket.on('deactivatebuzzers', function() {
        $('#board-container').addClass('bg-dark').removeClass('bg-light');
    });
    socket.on('categories', function (categories) {
        $('#c1').html(categories[0]);
        $('#c2').html(categories[1]);
        $('#c3').html(categories[2]);
        $('#c4').html(categories[3]);
        $('#c5').html(categories[4]);
        $('#c6').html(categories[5]);
    });
    socket.on('showboard', function () {
        $('#board').show();
        $('#clue').hide();
    });
    socket.on('displayclue', function (clue) {
        $('#clueText').html(clue);
        $('#clue').show();
        $('#board').hide();
    });
    socket.on('hideboardslot', function (boardIndex) {
        $('#' + boardIndex).html('');
    });
    socket.on('displayfinalcategory', function (category) {
        $('#board > tbody > tr > td').html('');
        $('#board > thead > tr > th').html('<br><br><br>');
        $('#43').html('<div class="text-white">' + category + '</div>');
    });
    socket.on('displayfinalclue', function (clue) {
        $('#44').html('<div class="text-white">' + clue + '</div>');
    });
});