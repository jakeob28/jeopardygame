$(function () {
    $('#normalj').hide();
    $('#finalj').hide();

    var socket = io.connect();
    console.log(socket);
    console.log('Checking start status...');
    socket.emit('checkstart');
    socket.on('startstatus', function(started) {
        if (started) {
            $('#content').html('<div class="alert alert-danger" role="alert">Error: Game already started</div>');
        }
    });
    socket.on('startj', function() {
        $('#prej').hide();
        $('#finalj').hide();
        $('#normalj').show();
    });
    socket.on('showfinalj', function () {
        $('#prej').hide();
        $('#normalj').hide();
        $('#finalj').show();
    });
    $(document).on('input', '#wager', function () {
        socket.emit('wager', getCookie("name"), $('#wager').val())
    });
    $(document).on('input', '#response', function () {
        socket.emit('response', getCookie("name"), $('#response').val())
    });
    socket.on('timeout', function() {
        $('#buzzer').addClass('btn-danger').removeClass('btn-success').addClass('ignoreBuzz');
        setTimeout(function () {
            $('#buzzer').addClass('btn-success').removeClass('btn-danger').removeClass('ignoreBuzz');
        }, 500);
    });
    socket.on('balances', function(balances) {
        console.log(balances);
        if ($('#balancetable').length) { //if div exists; i.e. if player is in game
            console.log('ingame');
            var table = '<table class="table"><thead><tr>';
            Object.keys(balances).forEach(function (key) {
                table = table.concat('<th>' + key + '</th>');
            });
            table = table.concat('</tr></thead><tbody><tr>');
            Object.keys(balances).forEach(function (key) {
                table = table.concat('<td>$' + balances[key] + '</td>');
            });
            table = table.concat('</tr></tbody></table>');
            console.log(table);
            $('#balancetable').html(table);
        }
    });
    socket.on('displayfinalclue', function () {
        $('#wager').prop('disabled', true);
    });
    socket.on('startfinal', function () {
        $('#response').prop('disabled', false);
    });
    socket.on('stopfinal', function () {
        $('#response').prop('disabled', true);
    });
    $('#join').click(function(){
        var name = $('#name').val();
        if (name !== "") {
            socket.emit('setname', name);
            document.cookie = "name=" + name;
            $('#prej').html('<h1>Jeopardy</h1><div class="alert alert-primary" role="alert">Waiting for game to start...</div>');
        }
    });

    $(document).on('click', '#buzzer', function () {
        if(!$('#buzzer').hasClass('ignoreBuzz'))
            socket.emit('buzz', getCookie("name"));
    });

    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length === 2) return parts.pop().split(";").shift();
    }
});