# Jeopardy Game
## What is it?
This JS application allows players to play jeopardy. Features include:
* Buzzers for contestants
* Custom clues defined in `data.json`
* Sound effects
* A fully functional board

## How to install
1. Install node.js using the instructions at [nodejs.org](https://nodejs.org/en/download/)
1. Clone this repo to your server
1. Navigate to the project directory in your preferred terminal, and install the required modules using with `npm install`
1. Run the project with `node app.js`. For development you can use `node run devstart`, which will automatically restart the server when any files are changed.

## How to use
1. Players will navigate to IP:PORT in their browser, where IP is the IP of the server, and PORT is the environment variable `process.env.PORT`. If this is not set, the app will fall back to port 80
1. The host will navigate to IP:PORT/admin
1. The board is displayed at IP:PORT/board, and should be in an area where all players can see.