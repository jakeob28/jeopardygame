var express = require('express');
var router = express.Router();
var path = require('path');


router.get('/', function(req, res, next) {
    res.render('player');
});
router.get('/admin', function(req, res){
    res.render('admin');
});
router.get('/board', function(req, res){
    res.render('board');
});
router.get('/js/howler.js', function(req, res){
    res.sendFile(path.resolve(__dirname + '/../node_modules/howler/dist/howler.js'));
});
router.get('/js/socket.io.js', function(req, res){
    res.sendFile(path.resolve(__dirname + '/../node_modules/socket.io-client/dist/socket.io.js'));
});
module.exports = router;
