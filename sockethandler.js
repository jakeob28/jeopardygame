var socket_io = require('socket.io');
var fs = require('fs');
var io = socket_io();
var socketApi = {};

socketApi.io = io;

var started = false;
var buzzersActive = false;
var balances = {};
var acceptFinal = false;

io.on('connection', function(socket){
    socket.emit('categories',getCategoryNames());

    io.emit('balances', balances);

    socket.on('checkstart', function() {
        socket.emit('startstatus', started);
        console.log("Start checked as " + started);
    });
    socket.on('setname', function(name) {
        balances[name] = 0;
    });
    socket.on('start', function () {
        io.emit('startj');
        io.emit('balances', balances);
        started = true;
    });
    socket.on('buzz', function (name) {
        if (!buzzersActive) {
            socket.emit('timeout');
        } else {
            buzzersActive = false;
            io.emit('deactivatebuzzers');
            io.emit('firstbuzz', name)
        }
    });
    socket.on('activatebuzzers', function () {
        buzzersActive = true;
        io.emit('activatebuzzers');
    });
    socket.on('deactivatebuzzers', function () {
        buzzersActive = false;
        io.emit('deactivatebuzzers');
    });
    socket.on('showboard', function () {
        io.emit('showboard');
    });
    socket.on('displayclue', function (boardIndex) {
        var raw = fs.readFileSync('data.json');
        var json = JSON.parse(raw);
        var cat = json.game.categories[boardIndex.charAt(0) - 1];
        var clueID = json.categories[cat].clues[boardIndex.charAt(1) - 1];
        var clue = json.clues[clueID].clue;
        var answer = json.clues[clueID].answer;
        io.emit('displayclue', clue);
        io.emit('hideboardslot', boardIndex);
        io.emit('answer', answer);
    });
    socket.on('setbalance', function (name, balance) {
        balances[name] = balance;
        io.emit('balances', balances);
    });
    socket.on('givemoney', function (name, balance) {
        balances[name] = parseInt(balances[name]) + balance;
        io.emit('balances', balances);
    });
    socket.on('showfinalj', function () {
        var raw = fs.readFileSync('data.json');
        var json = JSON.parse(raw);
        var clueID = json.game.final;
        var category = json.finals[clueID].category;
        io.emit('showfinalj');
        io.emit('displayfinalcategory', category);
    });
    socket.on('showfinalclue', function () {
        var raw = fs.readFileSync('data.json');
        var json = JSON.parse(raw);
        var clueID = json.game.final;
        var clue = json.finals[clueID].clue;
        var answer = json.finals[clueID].answer;
        io.emit('displayfinalclue', clue, answer);
    });
    socket.on('wager', function (name, wager) {
        io.emit('wager', name, wager);
    });
    socket.on('startfinal', function () {
        io.emit('startfinal');
        acceptFinal = true;
        setTimeout(function () {
            io.emit('stopfinal');
            acceptFinal = false;
        }, 30000);
    });
    socket.on('response', function (name, response) {
        io.emit('response', name, response);
    });
});

function getCategoryNames() {
    var raw = fs.readFileSync('data.json');
    var json = JSON.parse(raw);
    var catsIDs = json.game.categories;
    var catNames = [];
    catsIDs.forEach(function (id) {
        catNames.push(json.categories[id].title);
    });
    return catNames;
}

module.exports = socketApi;